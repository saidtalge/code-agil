import oracledb

class LeituraOrdemBancaria:
    def __init__(self):
        self.FILE = '/var/www/web_services_sia_banco_brasil/ordembancaria/22406645_OBN350.bco001.2022110400016584.xgovmar.c104372541'
    def opened(self):
        opened = open(self.FILE, mode='r', encoding='latin1').read().split('\n')
        return opened
    def read(self):
            for i in range(len(self.opened())):
                typeLine = self.opened()[i][:1]
                if(typeLine == '0'):
                    self.header(i)
                if(typeLine == '2'): #ob do tipo 2
                    self.ordembancaria(i)
                if(typeLine == '3'): #ob do tipo 3
                    self.ordembancariafaturalista(i)
                if(typeLine == '4'): #ob do tipo 4
                    self.ordembancariafatura(i)
                if(typeLine == '5'): #ob do tipo 3
                    self.ordembancariapagamentosemcodigodebarra(5)
    def ordembancaria(self, i):
        line = {
                'tipo': 2,
                'codigo_agencia_bancaria': self.opened()[i][1:6],
                'codigo_ug': self.opened()[i][6:12],
                'codigo_gestao': self.opened()[i][12:17],
                'codigo_re': self.opened()[i][17:28],
                'codigo_ob': self.opened()[i][28:39],
                'data_referencia': self.opened()[i][39:47],
                'codigo_operacao': self.opened()[i][51:53],
                'indicador_pagamento_pessoal': self.opened()[i][53:54],
                'valor_liquido_ob': self.opened()[i][63:80],
                'codigo_banco_favorecido': self.opened()[i][80:83],
                'codigo_agencia_favorecido': self.opened()[i][83:88],
                'codigo_conta_favorecido': self.opened()[i][88:98],
                'nome_favorecido': self.opened()[i][98:143],
                'endereco_favorecido': self.opened()[i][143:200],
                'codigo_ispb': self.opened()[i][200:208],
                'municipio_favorecido': self.opened()[i][208:236],
                'codigo_gru_deposito': self.opened()[i][236:253],
                'cep_favorecido': self.opened()[i][253:261],
                'unidade_federacao_favorecido': self.opened()[i][261:263],
                'obs_ob': self.opened()[i][263:303],
                'indicador_tipo_pagamento': self.opened()[i][303:304],
                'tipo_favorecido': self.opened()[i][304:305],
                'codigo_favorecido': self.opened()[i][305:319],
                'prefixo_agencia_dv_debito': self.opened()[i][319:324],
                'numero_conta_dv_debito': self.opened()[i][324:334],
                'finalidade_pagamento': self.opened()[i][334:337],
                'numero_sequencial_arquivo': self.opened()[i][343:350]
        }
        print(line)  
    def ordembancariafatura(self, i):
        line = {
                'tipo': 4,
                'codigo_agencia_bancaria': self.opened()[i][1:6],
                'codigo_ug': self.opened()[i][6:12],
                'codigo_gestao': self.opened()[i][12:17],
                'codigo_re': self.opened()[i][17:28],
                'codigo_ob': self.opened()[i][28:39],
                'data_referencia': self.opened()[i][39:47],
                'codigo_operacao': self.opened()[i][51:53],
                'numero_sequencial_item_lista': self.opened()[i][54:60],
                'valor_liquido_ob': self.opened()[i][63:80],
                'tipo_de_fatura': self.opened()[i][95:96],
                'conforme_redefinicao_abaixo': self.opened()[i][96:199],
                'tipo_identificacao_beneficiario': self.opened()[i][199:200],
                'codigo_favorecido': self.opened()[i][200:214],
                'gru_simples_valor_outras_deducoes': self.opened()[i][214:225],
                'gru_simples_valor_mora_multa': self.opened()[i][225:236],
                'gru_simples_valor_juros_encargos': self.opened()[i][236:247],
                'gru_simples_valor_outros_acrescimos': self.opened()[i][247:258],
                'obs_ob': self.opened()[i][263:303],
                'prefixo_agencia_dv_debito': self.opened()[i][319:324],
                'numero_conta_dv_debito': self.opened()[i][324:334],
                'finalidade_pagamento': self.opened()[i][334:337],
                'numero_sequencial_arquivo': self.opened()[i][343:350]
        }
        print(line)  
    def ordembancariafaturalista(self, i):
        line = {
                'tipo': 3,
                'codigo_agencia_bancaria': self.opened()[i][1:6],
                'codigo_ug': self.opened()[i][6:17],
                'codigo_re': self.opened()[i][17:28],
                'codigo_ob': self.opened()[i][28:39],
                'data_referencia': self.opened()[i][39:47],
                'codigo_operacao': self.opened()[i][51:53],
                'tipo_de_pagamento': self.opened()[i][53:54],
                'numero_sequencial_item_lista': self.opened()[i][54:60],
                'valor_liquido_ob': self.opened()[i][60:80],
                'codigo_banco_favorecido': self.opened()[i][80:83],
                'codigo_agencia_favorecido': self.opened()[i][83:87],
                'digito_verificador_agencia_bancaria': self.opened()[i][87:88],
                'codigo_conta_corrente_favorecido': self.opened()[i][88:97],
                'digito_conta_corrente_favorecido': self.opened()[i][97:98],
                'nome_favorecido': self.opened()[i][98:143],
                'endereco_favorecido': self.opened()[i][143:208],
                'municipio_favorecido': self.opened()[i][208:236],
                'codigo_crud': self.opened()[i][236:253],
                'cep_favorecido': self.opened()[i][253:261],
                'uf_favorecido': self.opened()[i][261:263],
                'obs_ob': self.opened()[i][263:303],
                'tipo_favorecido': self.opened()[i][304:305],
                'codigo_favorecido': self.opened()[i][305:319],
                'prefixo_agencia_dv_debito': self.opened()[i][319:324],
                'numero_conta_dv_debito': self.opened()[i][324:334],
                'codigo_retorno_operacao': self.opened()[i][341:343],
                'numero_sequencial_arquivo': self.opened()[i][343:350]
        }
        print(line)  
    def ordembancariapagamentosemcodigodebarra(self, i):
        line = {
            'tipo' : 5,
            'codigo_agencia_bancaria_ug' : self.opened()[i][1:6],
            'codigo_ug' : self.opened()[i][6:17],
            'codigo_re' : self.opened()[i][17:28],
            'codigo_ob' : self.opened()[i][28:39],
            'data_referencia' : self.opened()[i][39:47],
            'codigo_operacao' : self.opened()[i][51:53],
            'numero_sequencial_lista' : self.opened()[i][54:60],
            'valor_pagamento' : self.opened()[i][63:80],
            'data_pagamento' : self.opened()[i][80:88],
            'tipo_pagamento' : self.opened()[i][95:96]
        }
        if(line['tipo_pagamento'] == '1'):
            line['codigo_receita_tributo'] = self.opened()[i][96:102]
            line['codigo_identificacao_tributo'] = self.opened()[i][102:104]
            line['mes_ano_competencia'] = self.opened()[i][104:110]
            line['valor_previsto_pagamento_inss'] = self.opened()[i][110:127]
            line['valor_outras_entidades'] = self.opened()[i][127:144]
            line['atualizacao_monetaria'] = self.opened()[i][144:161]
        if(line['tipo_pagamento'] == '2'):
            line['codigo_receita_tributo'] = self.opened()[i][96:102]
            line['codigo_identificacao_tributo'] = self.opened()[i][102:104]
            line['periodo_apuracao'] = self.opened()[i][104:112]
            line['numero_referencia'] = self.opened()[i][112:129]
            line['valor_principal'] = self.opened()[i][129:146]
            line['valor_multa'] = self.opened()[i][146:163]
            line['valor_juros_encargos'] = self.opened()[i][163:180]
            line['data_vencimento'] = self.opened()[i][180:188]
        if(line['tipo_pagamento'] == '3'):
            line['codigo_receita_tributo'] = self.opened()[i][96:102]
            line['codigo_identificacao_tributo'] = self.opened()[i][102:104]
            line['periodo_apuracao'] = self.opened()[i][104:112]
            line['valor_receita_bruta_acumulada'] = self.opened()[i][112:129]
            line['percentual_receita_bruta_acumulada'] = self.opened()[i][129:136]
            line['valor_principal'] = self.opened()[i][136:153]
            line['valor_multa'] = self.opened()[i][153:170]
            line['valor_juros_encargos'] = self.opened()[i][170:187]
        print(line)  
    def header(self,i):
        line = {
                'tipo': 0,
                'data_criacao': self.opened()[i][35:43],
                'hora_criacao': self.opened()[i][43:47],
                'numero_remessa': self.opened()[i][47:52],
                'codigo_cliente': self.opened()[i][52:58],
                'numero_contrato': self.opened()[i][58:67],
                'numero_retorno_remessa': self.opened()[i][67:72],
                'codigo_retorno_remessa': self.opened()[i][72:74],
                'motivo_deovulacao': self.opened()[i][74:118],
                'tipo_arquivo': self.opened()[i][118:124],
                'numero_sequencial_arquivo': self.opened()[i][343:350]
        }
        print(line)
    def ins(self):
        print('123')

if __name__ == '__main__':
    LeituraOrdemBancaria().ins()
    #LeituraOrdemBancaria().read()